# Changelog

# 2.3.1

* Fixed Discord Rich Presence not working in certain zones
* Updated dependencies
# 2.3.0

* Upon running a new version of the client, a changelog window now appears
* The main window now remembers its size and maximized status
* The main window's title now displays the client version
* Fixed an issue with the application icon being set incorrectly
* Updated dependencies

# 2.2.2

* Updated dependencies

# 2.2.1

* Updated dependencies
* Updated packaging mechanisms

# 2.2.0

* Updated to a backwards incompatible version of Electron

## 2.1.4

* Updated dependencies

## 2.1.3

* Updated dependencies
* Updated copyright year

## 2.1.2

* Changed Dev Tools shortcut to Ctrl/Cmd + Shift + I

## 2.1.1

* Added more Discord Rich Presence features (BETA)
  * Now properly changes status on log out and character select
  * Added graphics for character spirits
* Fixed a bug with the chat window staying open after sending a message (#1)

## 2.1.0

* Added Discord Rich Presence intergration (BETA)
  * Some functionality might be missing

## 2.0.3

* Fixed an issue with missing packages

## 2.0.2

* Client now starts on a "server portal" page
* The icon is now properly set on Linux
* The window title is now properly set
* F12 now opens the website's Dev Tools
* Popup windows don't have a menu anymore

## 2.0.1

* Yet another rewrite
* Now uses `<webview>`
* Can load addons

## 1.1.1

* Added an icon

## 1.1.0

* Ported code over from [isleward-electron](https://gitlab.com/vildravn/isleward-electron)
