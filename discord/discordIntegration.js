const {ipcMain} = require('electron');
const discordClient = require('discord-rich-presence')('389007859758923777');

ipcMain.on('discord-integration-playerupdate', function(event, playerInfo) {
    discordClient.updatePresence({
        details: `Level ${playerInfo.level} ${capitalizeFirstLetter(playerInfo.class)}`,
        state: `As "${playerInfo.name}"`,
        largeImageKey: playerInfo.zone.toLowerCase(),
        largeImageText: capitalizeFirstLetter(playerInfo.zone),
        smallImageKey: playerInfo.class
    });
});

ipcMain.on('discord-integration-menuupdate', function(event, details) {
    discordClient.updatePresence({
        details: details,
        largeImageKey: 'default'
    });
});

discordClient.on('connected', function() {
    discordClient.updatePresence({
        details: 'On Login Page',
        largeImageKey: 'default'
    });
});

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}